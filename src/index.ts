import * as dotenv from "dotenv";
dotenv.config();
import * as express from "express";
import routes from "./routes";
import * as cors from "cors";
import * as morgan from "morgan";
import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";

const app = express();
const port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(morgan("dev"));

app.listen(port);

app.use(cors({ origin: true, credentials: true }));
app.get("/", (req, res: any) => {
  res.json("Wetlcome To my API service for test flutter Project");
});
app.use("/api/v1", routes());

console.log(`Express is listening on port ${port}`);
