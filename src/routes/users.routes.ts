import { Router } from 'express';
import { login, register, getUser } from '../controllers/usersController';
import { verifyToken } from '../utils/authentoken';
export default () => {
    const UsersRouter = Router()    
    UsersRouter.post('/register', register)
    UsersRouter.post('/login', login)
    UsersRouter.get('/',verifyToken, getUser)
    return UsersRouter
}