import { Router } from 'express'
import usersRoutes from './users.routes'

export default () => {
    const route = Router()
    route.use("/users", usersRoutes())
    route.use("/", (req, res: any) => {
        res.json("Welcome to API V1")
    })
    return route
}