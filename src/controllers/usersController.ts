import { signToken } from "./../utils/authentoken";
import { Request, Response, NextFunction } from "express";
import userService from "../service/users.service";
import { hash, verify } from "../utils/crypto";

export const register = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email, password, confirmPassword, username, firstname, lastname } =
    req.body;
  if (!password) return res.status(404).json("Please fill password");
  if (!confirmPassword)
    return res.status(404).json("Please fill confirmPassword");
  if (!username) return res.status(404).json("Please fill username");
  if (!firstname) return res.status(404).json("Please fill firstname");
  if (!lastname) return res.status(404).json("Please fill lastname");
  if (password != confirmPassword)
    return res.status(500).json("Password and Confirm Password Not Match");
  try {
    const hashPassword = await hash(password);
    const payload = {
      email,
      password: hashPassword,
      username,
      firstname,
      lastname,
    };
    const checkEmail = await userService.getUserByEmail(email);
    if (checkEmail) return res.status(401).json("Email Already Exit");
    const dataReturn = await userService.create(payload);
    res.status(200).json(dataReturn);
  } catch (error) {
    console.log("ERROR =>", error);
    res.status(500).json(error);
  }
};

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email, password } = req.body;
  try {
    console.log(req.body);
    
    const userExit = await userService.getUserByEmail(email);
    if (!userExit) return res.status(404).json("Email Not Found");
    const verifyPassword = await verify(password, userExit.password);
    if (verifyPassword == false)
      return res.status(401).json("Password Incorrect");
    const payload = {
      email: userExit.email,
      username: userExit.username,
      firstname: userExit.firstname,
      lastname: userExit.lastname,
    };
    const createToken = await signToken(payload);
    return res.status(200).json(createToken);
  } catch (error) {
    console.log("ERROR =>", error);
    res.status(500).json(error);
  }
};

export const getUser = async (req: any, res: Response, next: NextFunction) => {
  try {
    const email = req.user.email;
    const userExit = await userService.getUserByEmail(email);
    if (!userExit) return res.status(404).json("User Not Found");
    const payload = {
      email: userExit.email,
      username: userExit.username,
      firstname: userExit.firstname,
      lastname: userExit.lastname,
    };
    console.log(payload);
    
    return res.status(200).json(payload);
  } catch (error) {
    console.log("ERROR =>", error);
    res.json(error);
  }
};
