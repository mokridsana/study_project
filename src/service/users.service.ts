import { Request, NextFunction, Response } from "express";
import { Client, Pool } from "pg";
let PORT = process.env.RDS_PORT as unknown as number;
const client = new Client({
  host: process.env.RDS_HOSTNAME,
  user: process.env.RDS_USERNAME,
  password: process.env.RDS_PASSWORD,
  port: PORT,
  database: process.env.RDS_DB_NAME,
});
client.connect((err) => {
  if (err) {
    console.log("Connection Error =>", err.stack);
  } else {
    console.log("Connected");
  }
});
// const pool = new Pool(connection);
// console.log(pool);

export default {
  findAll: async (res: Response) => {
    client.query("SELECT * FROM users ORDER BY id ASC", (err, results: any) => {
      if (err) {
        throw err;
      }
      const dataReturn = { data: results.rows };
      return dataReturn;
    });
  },

  create: async (payload: any) => {
    const email = payload.email;
    const username = payload.username;
    const firstname = payload.firstname;
    const lastname = payload.lastname;
    const password = payload.password;
    console.log({ payload });

    return client
      .query(
        `INSERT INTO users (email, password,username,
        firstname,
        lastname) VALUES($1, $2, $3, $4, $5) RETURNING *`,
        [email, password, username, firstname, lastname]
      )
      .then((response) => {
        console.log("SAVE USER!!");
        return response.rows;
      });
  },

  getUserByEmail: async (email: any) => {
    return client
      .query("SELECT * FROM users WHERE email=$1", [email])
      .then((response) => {
        return response.rows[0];
      });
  },
};
