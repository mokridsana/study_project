import * as jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from 'express';

export const signToken = async (payload: any) => {
    const private_key = process.env.PRIVATE_KEY
    const token = jwt.sign(payload, private_key ,{ expiresIn: '24h'})
    return token
}

export const verifyToken = async (req: any, res: Response,next: NextFunction) => {    
    const bearerHeaders = req.headers.authorization
    const bearerToken = bearerHeaders.split(' ');
    const token = bearerToken[1]
    if(!token) return res.status(403).json("A token is required for authentication");
    try {
        const decoded = jwt.verify(token, process.env.PRIVATE_KEY);
        req.user = decoded;
    } catch (error) {
        return res.status(401).json("Invalid Token");
    }
    next()
}