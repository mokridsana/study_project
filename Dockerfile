FROM node:lts-alpine3.17

WORKDIR /usr/src/app

# Copy package.json for npm install
COPY package*.json /usr/src/app/
COPY . /usr/src/app
RUN yarn
# RUN yarn global add nodemon
RUN yarn global add pm2
#set timezone
# RUN echo "Asia/Bangkok" > /etc/timezone
# RUN rm -f /etc/localtime
# RUN dpkg-reconfigure -f noninteractive tzdata
COPY . /usr/src/app
CMD [ "yarn", "run", "dev" ]
